require eeze.inc

PR = "${INC_PR}.0"

SRC_URI = "\
  ${E_MIRROR}/${SRCNAME}-${PV}.tar.gz \
"

SRC_URI[md5sum] = "8fa2aa89ba48f262700a3cf88cb1c777"
SRC_URI[sha256sum] = "d8ffb1af53c03c5860cd90ae78aa5ae0ce2d027a462509deaca5ee3adacd0cee"
