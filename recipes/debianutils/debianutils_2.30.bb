# Copyright (C) 2007, Stelios Koroneos - Digital OPSiS, All Rights Reserved
# Released under the MIT license (see packages/COPYING)

require debianutils.inc

PR = "${INC_PR}"

do_configure_prepend() {
	sed -i -e 's:tempfile.1 which.1:which.1:g' Makefile.am
}

SRC_URI[md5sum] = "0485d8319d7588b328db3b15e33629ad"
SRC_URI[sha256sum] = "596102c4b1d2db7f7d2b3485f11ce07235015b4d26f5a59da8b29058fadfc993"
